﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV4_Z1
{
    class Adapter:IAnalytics
    {
        private Analyzer3rdParty analyticsService;

        public Adapter(Analyzer3rdParty service)
        {
        this.analyticsService = service;
        }
        private double[][] ConvertData(Dataset dataset)
        {
            //implementation missing!
            
            int rowCount = dataset.GetData().Count;
            int columnCount = 0;
            int i, j;
            double[][] array = new double[rowCount][];

            foreach (var data in dataset.GetData())
            {
                columnCount = 0;
                foreach(var variable in data)
                {
                    columnCount++;
                }
            }
            
            for (i = 0; i < rowCount; i++)
            {
                array[i] = new double[columnCount];
            }

            i = 0;
            foreach (var data in dataset.GetData())
            {
                j = 0;
                foreach (var variable in data)
                {
                    array[i][j] = variable;
                    j++;
                }
                i++;
            }

            return array;
        }
        public double[] CalculateAveragePerColumn(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerColumnAverage(data);
        }
        public double[] CalculateAveragePerRow(Dataset dataset)
        {
            double[][] data = this.ConvertData(dataset);
            return this.analyticsService.PerRowAverage(data);
        }

    }
}
