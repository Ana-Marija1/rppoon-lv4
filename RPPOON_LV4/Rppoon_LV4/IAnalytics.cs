﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV4_Z1
{
    interface IAnalytics
    {
        double[] CalculateAveragePerColumn(Dataset dataset);
        double[] CalculateAveragePerRow(Dataset dataset);

    }
}
