﻿using System;

namespace Rppoon_LV4_Z1
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zadatak 1.
            //Implementirati metode iz primjera 1.3 kojima nedostaje implementacija.
            //Zadatak 2.
            //Testirati opisane klase.Za testiranje koristiti datoteku sadržaja koji je dan u nastavku.
            
            Dataset dataset = new Dataset();
            string FilePath = "D:\\Ana-Marija\\Faks\\IV. semestar\\RPPOON\\RPPOON_LV4\\Rppoon_LV4\\CSVFile.txt"; 
            dataset.LoadDataFromCSV(FilePath);
            Analyzer3rdParty analyzer = new Analyzer3rdParty();
            IAnalytics analytics = new Adapter(analyzer);
            
            Console.WriteLine("Calculated average per column: ");
            Print(analytics.CalculateAveragePerColumn(dataset));
            Console.WriteLine("Calculated average per row: ");
            Print(analytics.CalculateAveragePerRow(dataset));
        }

        static void Print(double[] array)
        {
            foreach(double variable in array)
            {
                Console.WriteLine(Convert.ToDecimal(variable));
            }
        }
    }
}
