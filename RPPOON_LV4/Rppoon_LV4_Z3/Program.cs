﻿using System;
using System.Collections.Generic;

namespace Rppoon_LV4_Z3
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zadatak 3.
            //Dodati još jednu klasu koja predstavlja knjigu, po uzoru na klasu koja predstavlja video.Knjiga ima atribute
            //za naslov, i osnovnu cijenu od 3.99.Ugraditi sučelje IRentable. Stvoriti listu i u nju dodati jednu knjigu i jedan
            //film te ju ispisati pomoću priloženog printera(klasa RentingConsolePrinter).

            List<IRentable> rentable = new List<IRentable>()
            {
                new Book("I Am Legend"),
                new Video("Train to Busan")
            };

            RentingConsolePrinter printer = new RentingConsolePrinter();
            Console.WriteLine("Items for rent: ");
            printer.DisplayItems(rentable);
            Console.WriteLine("Total price of renting: ");
            printer.PrintTotalPrice(rentable);

        }
    }
}
