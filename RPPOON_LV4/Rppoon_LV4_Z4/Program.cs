﻿using System;
using System.Collections.Generic;

namespace Rppoon_LV4_Z4
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zadatak 4.
            //U listu iz prošlog zadatka dodati po jednu hit knjigu i jedan hit film i ponoviti ispis.Uočavate li ikakve razlike?
            //Nema razlike jer dekorater omogućuje dinamičko dodavanje novog ponašanja i stanja individualnim objektima tijekom
            //izvođenja, ali tako da ostanu kompatibilni s postojećim kodom.
            //Ako se pita ima li razlike u ispisu objekata, objekti klase HotItem koštaju dodatno 1.99 i njihov ispis opisa
            //počinje s "Trending:".

            Book book = new Book("American Dirt");
            HotItem hotBook = new HotItem(book);
            Video video = new Video("Parasite");
            HotItem hotVideo = new HotItem(video);

            List<IRentable> rentable = new List<IRentable>()
            {
                new Book("I Am Legend"),
                new Video("Train to Busan"),
                hotBook,
                hotVideo
            };
            

            RentingConsolePrinter printer = new RentingConsolePrinter();
            Console.WriteLine("Items for rent: ");
            printer.DisplayItems(rentable);
            Console.WriteLine("Total price of renting: ");
            printer.PrintTotalPrice(rentable);
        }
    }
}
