﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV4_Z5
{
    class DiscountedItem:RentableDecorator
    {
        private readonly double DiscountedItemDiscount;
        public DiscountedItem(IRentable rentable,double discount) : base(rentable)
        {
            this.DiscountedItemDiscount = discount;
        }
        public override double CalculatePrice()
        {
            return Math.Round(base.CalculatePrice()*(1-DiscountedItemDiscount),2);
        }
        public override String Description
        {
            get
            {
                return base.Description + string.Format(" now at {0}% off!", (DiscountedItemDiscount * 100).ToString());
            }
        }
    }
}
