﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV4_Z5
{
    interface IRentable
    {
        String Description { get; }
        double CalculatePrice();
    }
}
