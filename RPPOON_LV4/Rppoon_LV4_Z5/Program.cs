﻿using System;
using System.Collections.Generic;

namespace Rppoon_LV4_Z5
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zadatak 5.
            //Po uzoru na HotItem dodati klasu DiscountedItem koja predstavlja artikle na popustu. Naslijediti
            //odgovarajuću klasu i prepisati(engl. override) odgovarajuće metode osnovne klase.Klasa ima atribut za
            //postotak popusta, koji se rabi u metodama propisanim sučeljem tako da se cijena umanjuje za dani postotak,
            //a u opis se uključuje „now at [XY]% off!“ nakon ispisivanja osnovnog opisa.
            //Za testiranje nove funkcionalnosti dodati novu listu IRentable objekata na popustu pod nazivom flashSale.
            //Dodati sve elemente iz osnovne liste u novu, ali tako da budu na popustu koji postavite na neku vrijednost po
            //vlastitom izboru.
           
            Book book = new Book("American Dirt");
            HotItem hotBook = new HotItem(book);
            Video video = new Video("Parasite");
            HotItem hotVideo = new HotItem(video);

            List<IRentable> rentable = new List<IRentable>()
            {
                new Book("I Am Legend"),
                new Video("Train to Busan"),
                hotBook,
                hotVideo
            };

            List<IRentable> flashSale = new List<IRentable>()
            {
                new DiscountedItem(rentable[0], 0.6),
                new DiscountedItem(rentable[1], 0.5),
                new DiscountedItem(rentable[2], 0.2),
                new DiscountedItem(rentable[3], 0.15)
            }; 

            RentingConsolePrinter printer = new RentingConsolePrinter();
            Console.WriteLine("Items for rent: ");
            printer.DisplayItems(flashSale);
            Console.WriteLine("Total price of renting: ");
            printer.PrintTotalPrice(flashSale);
        }
    }
}
