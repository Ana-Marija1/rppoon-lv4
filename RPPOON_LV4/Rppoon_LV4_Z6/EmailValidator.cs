﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV4_Z6
{
    class EmailValidator:IEmailValidatorService
    {
        public bool IsValidAddress(String candidate)
        {
            if (String.IsNullOrEmpty(candidate))
            {
                return false;
            }
            return ContainsSignAt(candidate) && EndsWithComOrHr(candidate);
        }
        private bool ContainsSignAt(String candidate)
        {
            return candidate.Contains("@");
        }
        private bool EndsWithComOrHr(String candidate)
        {
            return candidate.EndsWith(".com") || candidate.EndsWith(".hr");
        }
    }
}
