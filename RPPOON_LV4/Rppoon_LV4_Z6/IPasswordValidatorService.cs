﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV4_Z6
{
    interface IPasswordValidatorService
    {
        bool IsValidPassword(String candidate);
    }
}
