﻿using System;

namespace Rppoon_LV4_Z6
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zadatak 6.
            //Napisati klasu EmailValidator koja ugrađuje sučelje IEmailValidatorService prema uzoru na klasu
            //PasswordValidator.E - mail je valjan ako sadrži znak „@“ i ako završava ili na „.com“ ili na „.hr“. U glavnoj
            //funkciji kreirajte testne stringove za lozinku i e-mail te testirajte napisane klase.

            String password1 = "pDbf5Zg8";
            String password2 = "password";
            String e_mail1 = "EEwzHH@mailgator.org";
            String e_mail2 = "pero.peric@gmail.com";

            PasswordValidator passwordValidator = new PasswordValidator(8);
            EmailValidator emailValidator = new EmailValidator();

            Print(passwordValidator.IsValidPassword(password1), emailValidator.IsValidAddress(e_mail1));
            Print(passwordValidator.IsValidPassword(password2), emailValidator.IsValidAddress(e_mail2));
            Print(passwordValidator.IsValidPassword(password1), emailValidator.IsValidAddress(e_mail2));
            Print(passwordValidator.IsValidPassword(password2), emailValidator.IsValidAddress(e_mail1));
        }

        static void Print(bool password,bool mail)
        {
            if (password && mail)
            {
                Console.WriteLine("Both password and e-mail are valid!");
            }
            else if(password && !mail)
            {
                Console.WriteLine("Password is valid, but e-mail is not!");
            }
            else if(!password && mail)
            {
                Console.WriteLine("E-mail is valid, but password is not!");
            }
            else
            {
                Console.WriteLine("Both password and e-mail are not valid!");
            }
        }
    }
}
