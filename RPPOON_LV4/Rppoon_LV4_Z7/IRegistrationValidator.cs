﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rppoon_LV4_Z7
{
    interface IRegistrationValidator
    {
        bool IsUserEntryValid(UserEntry entry);
    }
}
