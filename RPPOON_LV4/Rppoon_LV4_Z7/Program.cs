﻿using System;

namespace Rppoon_LV4_Z7
{
    class Program
    {
        static void Main(string[] args)
        {
            //Zadatak 7.
            //Napisati konkretnu klasu za validaciju koja implementira sučelje IRegistrationValidator koje predstavlja
            //fasadu.Fasada treba kao atribute imati oba ranija validatora(lozinke i e-maila) koje može stvoriti unutar
            //podrazumijevanog konstruktora. U metodi koju ugrađuje iz sučelja IRegistrationValidator mora se osloniti
            //na oba atributa kako bi donijela odluku o tome je li korisnik ispravno unio podatke.
            //U glavnoj funkciji napišite program koji opetovano traži unos korisničkih podataka korištenjem UserEntry
            //klase sve dok provjera registracijskih podataka nije zadovoljena.
            
            RegistrationValidator validator = new RegistrationValidator();

            while (!validator.IsUserEntryValid(UserEntry.ReadUserFromConsole()))
            {
                Console.WriteLine("Registration failed. Invalid password or e-mail! Try again.");
            }
            Console.WriteLine("Registration succeeded. Welcome!");
        }
    }
}
